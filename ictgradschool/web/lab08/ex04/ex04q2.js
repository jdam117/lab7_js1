"use strict";

// Provided variables.
var year = 1990;

// Variables you'll be assigning to in this question.
var isLeapYear;

// TODO Your code for part (2) here.
if(year % 400 == 0 ){
    isLeapYear = true;
}else if(year % 100 == 0){
    isLeapYear = false;
}else if( year % 4 == 0){
    isLeapYear = true;
}else{
    isLeapYear = false;
}

// Printing the answer
if (isLeapYear) {
    console.log("Part 2: " + year + " is a leap year.");
} else {
    console.log("Part 2: " + year + " is NOT a leap year.");
}