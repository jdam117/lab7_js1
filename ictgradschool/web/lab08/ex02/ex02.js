"use strict";

// Provided variables
var amount = 3;
var userResponse = 'Y';
var firstName = "Bob";
var singer = "NOT Taylor Swift!";
var year = 1977;

// Example
var isOdd = (amount % 2 != 0);
console.log("isOdd = " + isOdd);

// TODO write your answers here
// Whether userResponse is equal to either y or Y.

if(userResponse == "y" || userResponse =="Y"){
    console.log("true");
}else{
    console.log("false");
}
// Whether firstName begins with the letter A.
if(firstName.charAt(0) == "a" || firstName.charAt(0) == "A"){
    console.log("true");
}else{
    console.log("false");
}
// Whether singer is equal to “Taylor Swift”.
if(singer == "Taylor Swift"){
    console.log("true");
}else{
    console.log("false");
}
// Whether yearBorn is greater than 1978, but not equal to 2013.
if(year > 1978 && year != 2013){
    console.log("true");
}else{
    console.log("false");
}